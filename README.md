# ZOSHOP
## Bắt đầu

1. Git clone develop branch
2. Composer install
3. Composer global require "fxp/composer-asset-plugin:dev-master"
4. ./init, chọn development environment

### Điều kiện tiên quyết

1. PHP, tối thiểu 5.4, các plugin php-mysql, php-gd, php-zip, php-xml, php-redis
2. Mạng internet để kết nối tới các database development chia sẻ

### Cài đặt

Một từng bước loạt các ví dụ mà cho bạn có để có được một môi trường phát triển.

## Thư viện sử dụng

1. Yii framework 2.x
2. Jquery 2.x, AngularJS 1.x, TextAngular, Bootstrap, AngularUI...

## Thành viên dự án

| Họ và tên            | Vị trí            | Email | Ghi chú |
|----------------------|-------------------|-------|---------|
|Ngô Thanh Vinh        | Creater           |       |         |

## Môi trường
* Production:
* Dev:


## Phiên bản

| Phiên bản | Ngày release | Thay đổi | Ghi chú |
|-----------|:------------:|---------:|---------|
| 0.1       |              |          |         |
| 0.2       |              |          |         |
| 0.3       |              |          |         |

## Tài liệu nghiệp vụ

* Yêu cầu (thư mục trên Google Drive)
