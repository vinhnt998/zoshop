<?php

namespace cms\widgets;

class Footer extends \yii\base\Widget {

    public function run() {
        return $this->render('footer');
    }

}
