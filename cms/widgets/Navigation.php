<?php

namespace cms\widgets;

class Navigation extends \yii\base\Widget {

    public function run() {
        return $this->render('navigation');
    }

}
