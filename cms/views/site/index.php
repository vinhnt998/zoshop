<?php

use yii\helpers\Url;

?>

<script type="text/javascript" src="//kjur.github.io/jsrsasign/jsrsasign-4.1.4-all-min.js"></script>
<script type="text/javascript" src="//kjur.github.io/jsjws/ext/json-sans-eval-min.js"></script>
<script type="text/javascript" src="//kjur.github.io/jsjws/jws-3.2.js"></script>

<script>
    (function (w, d, s, g, js, fs) {
        g = w.gapi || (w.gapi = {});
        g.analytics = {
            q: [], ready: function (f) {
                this.q.push(f);
            }
        };
        js = d.createElement(s);
        fs = d.getElementsByTagName(s)[0];
        js.src = 'https://apis.google.com/js/platform.js';
        fs.parentNode.insertBefore(js, fs);
        js.onload = function () {
            g.load('analytics');
        };
    }(window, document, 'script'));
</script>
<section class="content-header">
    <h1>
        Hệ thống quản trị
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= Url::home() ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Khách ghé thăm mới</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="chartNew"></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-lg-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Nguồn truy cập</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="chartSource"></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-lg-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Truy cập theo khu vực</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="chartGeo"></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Truy cập tháng qua</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="chartVisit"></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-lg-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Nội dung được quan tâm nhất</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="chartContent"></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

<script>
    var pHeader = {"alg": "RS256", "typ": "JWT"}
    var sHeader = JSON.stringify(pHeader);
    var pClaim = {};
    pClaim.aud = "https://www.googleapis.com/oauth2/v3/token";
    pClaim.scope = "https://www.googleapis.com/auth/analytics.readonly";
    pClaim.iss = "postmart-956@postmart.iam.gserviceaccount.com";
    pClaim.exp = KJUR.jws.IntDate.get("now + 1hour");
    pClaim.iat = KJUR.jws.IntDate.get("now");

    var sClaim = JSON.stringify(pClaim);
    var key = "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCrkKFJFW7KjIHq\ndl8r5SbaHAAft0OF7Bu4fOIDsHBbYtU+rmgSp/3ie/Ss/TGEPgYcXoxmhKirZevE\nXlr1h6gRflwvc/1/hkc1tYONW1QMMOASNDKaMP2972gebMlfT5/g3EdVURin7XY3\nyG9woBEV3L8CvqslQrG7z3G9vGkdyjg6ZhXgoc+WUgBMSTzwmdqC2t0Ubl0lxGmu\nP7yjK23yrmulLJhhqkmCxfDTWb3yAQ0GDQ19dq6gRCp4B19eMuuLdXA4a3IVYLiW\n4ae1UPshDT7FuKeTEHK3AaWtYjIfHqUSoQehcvOE3jKGxn15J/qSA6zyKnn2JnJH\nqTqrO74LAgMBAAECggEAFVW8gNVN/EPh1LGDX0w1UH1ClK8aZ1k3ufGpvOdgPt+5\nAKej9eH3LrGBzqYD1jFNBZJ1EjtKqwSB1QnIykIO5/IB7mgVrO9FWWFTqoH3dIv6\nvrjIAX24fY9RMCtmFVirsJpOspuNneBn9ELpfM94Sx2IPAP6+NIPk86VveW5YASg\nEt8oqaPPFjrfOjiZYB/3+TzLw/yQ34FmGMlG6o93eF+EnfOGD2gifTHK03dKp5w9\n5RumjHhZBKC6DKVUsfw+Wii83izbajhlUdPeZTGByR6ByQycvjoYrLbweWIF9Edp\nOy/m08JO49oE9c3DYhqwR2ROgP+ASiHmHUlro1KxJQKBgQDUfQxZPQufmJXu9HLu\n/S0L4pSz7cD/sYT4fDOtbzh9r3fhuK3ZJanXoAsFKtFRGwhsHPMXxNAC9idwqmrv\nDsW4ZO83uFqpnBTviAj/A8wehokmpifF/CSBZDNufFRkvNc06Za3HVf/XOyoPPVY\nAxCy3c7wkIWRUdICCC3H29nFbQKBgQDOsk//iAITAeXiHjJn38rZwqFGhclfe68Q\nzDWK6ZiQ7uDWaDCdhXexL4HW8da/FB9elZIxkTYyN5vinGc4yme9QUjDTP5aIfIy\nsD/ViBsStMxi0DPJ6lh9JI8HwU07mm6COrs6250ukPdSE9Q4A5krzB+RUneJODBu\n1CBfZoZ+VwKBgA6vKZhNKK15zPq7d+neIyvMF7yrdIf3C5pkcunMYtoEs0MtEQ6N\nlMQq8jgEMnzpYlaeVYpR9pIN0sHtdQBaQ/2vM/zkx5crCyNWglClQYjmgTnjh6Id\n/6PVedxrySVBC5pDfj/fTkJ+eHhT5hyT8aZ6v59OTXyya9HqIMfMAKutAoGAOjyR\nP5zxgKt7Or9pUk0CmWI2EGmneCigBaqTt2TjoIo9fQt0Dx3IwHpzKb2P1Nx1ji+0\nNhUE4Rc765hHmP+mRPN87dHmpDyGQfPYuHAVuKqKH5krzkshzXiceAHlodJ8KmMj\n14yC99jOZijCFVomMOndRxOmLFpRgxhvWpHHeRcCgYAjZIqpuwYGg8tnF6aBJxYt\nPuftvwXP/z/hzue+vwBmr7uKPkSl97DbCtpLNA4LYO6jAX7Rh9FRLm+4bAyHEGTd\njTMsR8Sv51XbZglWF9NuZsXT9lZAb/Lk1t4HtIrMoMI/JvtXgBd+euGrUB5jA7LM\n+/blFqITEIF7Is2EqQ/pyw==\n-----END PRIVATE KEY-----\n";
    var sJWS = KJUR.jws.JWS.sign(null, sHeader, sClaim, key);
    var XHR = new XMLHttpRequest();
    var urlEncodedData = "";
    var urlEncodedDataPairs = [];

    urlEncodedDataPairs.push(encodeURIComponent("grant_type") + '=' + encodeURIComponent("urn:ietf:params:oauth:grant-type:jwt-bearer"));
    urlEncodedDataPairs.push(encodeURIComponent("assertion") + '=' + encodeURIComponent(sJWS));
    urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');

    // We define what will happen if the data are successfully sent
    XHR.addEventListener('load', function (event) {
        var response = JSON.parse(XHR.responseText);
        var token = response["access_token"];

        gapi.analytics.ready(function () {

            var view = "ga:190064491";
            gapi.analytics.auth.authorize({
                'serverAuth': {
                    'access_token': token
                }
            });

            new gapi.analytics.googleCharts.DataChart({
                query: {
                    'ids': view,
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday',
                    'metrics': 'ga:sessions',
                    'dimensions': 'ga:userType',
                    'sort': '-ga:sessions'
                },
                chart: {
                    'container': 'chartNew',
                    'type': 'PIE',
                    'options': {
                        'width': '100%'
                    }
                }
            }).execute();

            new gapi.analytics.googleCharts.DataChart({
                query: {
                    'ids': view,
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday',
                    'metrics': 'ga:sessions',
                    'dimensions': 'ga:sourceMedium',
                    'sort': '-ga:sessions',
                    'max-results': 10
                },
                chart: {
                    'container': 'chartSource',
                    'type': 'BAR',
                    'options': {
                        'width': '100%'
                    }
                }
            }).execute();

            new gapi.analytics.googleCharts.DataChart({
                query: {
                    'ids': view,
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday',
                    'metrics': 'ga:sessions',
                    'dimensions': 'ga:country',
                    'sort': '-ga:sessions',
                    'max-results': 100
                },
                chart: {
                    'container': 'chartGeo',
                    'type': 'GEO',
                    'options': {
                        'width': '100%'
                    }
                }
            }).execute();

            new gapi.analytics.googleCharts.DataChart({
                query: {
                    'ids': view,
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday',
                    'metrics': 'ga:sessions,ga:users',
                    'dimensions': 'ga:date'
                },
                chart: {
                    'container': 'chartVisit',
                    'type': 'LINE',
                    'options': {
                        'width': '100%'
                    }
                }
            }).execute();

            new gapi.analytics.googleCharts.DataChart({
                query: {
                    'ids': view,
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday',
                    'metrics': 'ga:pageviews',
                    'dimensions': 'ga:pagePathLevel1',
                    'sort': '-ga:pageviews',
                    'max-results': 10
                },
                chart: {
                    'container': 'chartContent',
                    'type': 'PIE',
                    'options': {
                        'width': '100%',
                    }
                }
            }).execute();

        });
    });

    // We define what will happen in case of error
    XHR.addEventListener('error', function (event) {
        console.log('Oops! Something went wrong.');
    });

    XHR.open('POST', 'https://www.googleapis.com/oauth2/v3/token');
    XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    XHR.send(urlEncodedData);
</script>
