var app = angular.module('app', ['ui.bootstrap', 'textAngular', 'fileDialog', 'angularMoment', 'chart.js', 'as.sortable', 'colorpicker.module', 'toggle-switch', 'AxelSoft', 'ui.bootstrap.datetimepicker']);

app.service('location', function ($uibModal) {
    this.getPos = function () {
        $.ajax({
            url: baseUrl + '/warehouse/searchmerchant',
            method: 'GET',
            data: {
                param: term,
            },
            success: function (resp) {
                loading.hide();
                if (resp.success) {
                    resp.data.splice(0, 0, {id: 0, name: 'Chọn bưu điện tỉnh'});
                    result = resp.data;
                    deferred.resolve(result);
                } else {
                    bootbox.alert(resp.message);
                }
            }
        });
    }

});

$.ajaxSetup({
    dataType: 'json',
    error: function (xhr, status, error) {
        loading.hide();
        if (xhr.status == 403) {
            bootbox.alert('Bạn không có quyền thực hiện hành động này');
        } else {
            bootbox.alert(error);
        }
    }
});

var loading = {};

loading.show = function () {
    if ($('#loading').length <= 0) {
        $('body').append('<div id="loading" style="display:none;" class="loading"></div>');
    }
    $('#loading').show();
};

loading.hide = function () {
    $('#loading').hide();
};

yii.confirm = function (message, ok, cancel) {
    bootbox.confirm(
        {
            message: message,
            callback: function (confirmed) {
                if (confirmed) {
                    !ok || ok();
                } else {
                    !cancel || cancel();
                }
            }
        }
    );
    return false;
};

$(document).ready(function () {
    setInterval(function () {
        $.ajax({
            url: baseUrl + '/site/ping',
            method: 'GET',
            success: function (resp) {
            }
        });
    }, 60000);
});

















app.service('modal', function ($uibModal) {
    this.alert = function (message) {
        $uibModal.open({
            animation: true,
            templateUrl: 'alertModal',
            controller: 'alert',
            resolve: {
                params: function () {
                    return {
                        message: message
                    }
                }
            }
        });
    };

    this.alertHtml = function (message) {
        $uibModal.open({
            animation: true,
            templateUrl: 'alertHtmlModal',
            controller: 'alertHtml',
            resolve: {
                params: function () {
                    return {
                        message: message
                    }
                }
            }
        });
    };
});

app.controller('alert', function ($scope, $uibModalInstance, params) {
    $scope.message = params.message;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

app.controller('alertHtml', function ($scope, $uibModalInstance, params) {
    $scope.message = params.message;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});