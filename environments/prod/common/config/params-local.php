<?php

return [
    'mqHost' => '171.244.49.167',
    'mqPort' => 5672,
    'mqUser' => 'admin',
    'mqPass' => 'Matkhau@123',
    'mqPrefix' => 'voso_',
    'imgServerUrls' => ['https://image.voso.vn'],
    'imgUser' => 'vosoimage',
    'imgPrivateKey' => 'imagesovo',
    'maxWeight' => '25000',
    'operatorNumberText' => '1900 1243',
    'operatorNumber' => 19001243,
    'frontendUrl' => 'voso.vn',
    'SMS_WSDL' => 'http://210.245.12.222:8088/voso/SendSMS?WSDL',
    'SMS_Username' => 'voso',
    'SMS_Password' => 'voso',
    'mailSender' => 'support@voso.vn',
    'mailSenderName' => 'Voso',
    'vosoCustomerId' => '11000A04000307000',
    //----moi truong that-----

    'vosoToken' => '8d0c43d4-f49b-49de-9663-47ce2567365c',
    'vosoStatusToken' => 'fa0698c6-23db-4327-b8d3-f545c841a7d0',
    'vosoStatusTokenTest' => 'ed90c5a6-69c7-4c17-8f18-1e9d5d699ee7',
    'googleAPIKey' => 'AIzaSyBscP1na7U74Epc4vCCFpgH3LvIE3_RgAc',
    'vosoEndpoint' => 'https://voso.voso.vn/serviceApi/v1',
    'vosoStatusEndpoint' => 'https://ctt.voso.vn/serviceApi/v1',

    //for test only
    // 'vosoToken' => 'ed90c5a6-69c7-4c17-8f18-1e9d5d699ee7',
    // 'vosoStatusToken' => '954ef2f1-9013-4030-ba69-f65a15cfb4d4',
    // 'vosoStatusTokenTest' => 'af7cdf1a-2397-4289-bdf8-cec472d8639f',
    // 'googleAPIKey' => 'AIzaSyBscP1na7U74Epc4vCCFpgH3LvIE3_RgAc',
    // 'vosoEndpoint' => 'https://voso.vn/demo/serviceApi/v1',
    // 'vosoStatusEndpoint' => 'https://voso.vn/demo/serviceApi/v1',
    //---
    
    'cacheDuration' => 3600,
    'cacheEnabled' => true,

    'viettelpay_uri' => 'https://pay.bankplus.vn:8450/PaymentGateway/payment',
    'viettelpay_access_code' => 'd41d8cd98f00b204e9800998ecf8427e16e556c1038fd9426e0e3a08a53bff4d',
    'viettelpay_hash_key' => 'd41d8cd98f00b204e9800998ecf8427eb3f8bb386d0d284cc075a6fed82b121a',
    'viettelpay_merchant_code' => 'VOSO',
    'viettelpay_merchant_name' => 'VOSO_VTP',
    'viettelpay_domain' => 'https://voso.vn',
];
