<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('v2', dirname(dirname(__DIR__)) . '/v2');
Yii::setAlias('cms', dirname(dirname(__DIR__)) . '/cms');
Yii::setAlias('oms', dirname(dirname(__DIR__)) . '/oms');
Yii::setAlias('mc', dirname(dirname(__DIR__)) . '/mc');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('report', dirname(dirname(__DIR__)) . '/report');
