<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'static/css/bootstrap.css',
        'static/css/font-awesome.css',
        'static/css/fonts.css',
        'static/css/owl.carousel.css',
        'static/css/jquery.mmenu.all.css',
        'static/css/animate.css',
        'static/css/cloudzoom.css',
      	'static/css/common.css',
      	'static/css/form.css',
      	'static/css/home.css',
      	'static/css/browse.css',
        'static/css/content.css',
        'static/css/detail.css',
        'static/css/checkout.css',
        'static/css/style-v2.min.css',
        'css/style.css',
        '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css'
    ];
    public $js = [
        'static/js/owl.carousel.js',
        'static/js/jquery.mmenu.min.all.js',
        'static/js/jquery.slimscroll.js',
        'static/js/jquery.nstSlider.js',
        'static/js/cloudzoom.js',
        'static/js/style.js',
        'static/js/style-v2.js',
        'js/lib/imboclient.min.js',
        'js/lib/bootbox.js',
        'js/lib/angular.min.js',
        'js/lib/angular-filedialog.js',
        'js/lib/angular-animate.min.js',
        'js/lib/angular-ui-bootstrap.min.js',
        'js/lib/moment.min.js',
        'js/lib/angular-moment.min.js',
        'js/lib/angular-local-storage.min.js',
        'js/inputmask.js',
        'js/utils.js',
        'js/app.js',
        'js/modal.js',
        'js/auth.js',
        'js/product.js',
        'js/order.js',
        'js/style.js',
        'js/user.js',
        'js/utils.js',
        'js/footer.js',
        'js/search.js',
        'js/home.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];

}
