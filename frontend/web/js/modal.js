app.service('modal', function ($uibModal) {
    this.alert = function (message) {
        $uibModal.open({
            animation: true,
            templateUrl: 'alertModal',
            controller: 'alert',
            resolve: {
                params: function () {
                    return {
                        message: message
                    }
                }
            }
        });
    };

    this.alertHtml = function (message) {
        $uibModal.open({
            animation: true,
            templateUrl: 'alertHtmlModal',
            controller: 'alertHtml',
            resolve: {
                params: function () {
                    return {
                        message: message
                    }
                }
            }
        });
    };
});

app.controller('alert', function ($scope, $uibModalInstance, params) {
    $scope.message = params.message;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

app.controller('alertHtml', function ($scope, $uibModalInstance, params) {
    $scope.message = params.message;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});