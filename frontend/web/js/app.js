var app = angular.module('app', ['fileDialog', 'ui.bootstrap', 'angularMoment', 'LocalStorageModule'])
        .directive('stringToNumber', function () {
            return {
                require: 'ngModel',
                link: function (scope, element, attrs, ngModel) {
                    ngModel.$parsers.push(function (value) {
                        return '' + value;
                    });
                    ngModel.$formatters.push(function (value) {
                        return parseFloat(value);
                    });
                }
            };
        });

$.ajaxSetup({
    dataType: 'json',
    error: function (xhr, status, error) {
        loading.hide();
        console.log(error);
    }
});

var loading = {};

loading.show = function () {
    if ($('#loading').length <= 0) {
        $('body').append('<div id="loading" class="loading"><div class="loading-inner"><span class="loading-img"></span><p>Vui lòng chờ trong giây lát</p></div></div>');
    }
    $('#loading').show();
};

loading.hide = function () {
    $('#loading').hide();
};


function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}

CloudZoom.quickStart();
$('.nstSlider').nstSlider({
    "rounding": {
        "1000000": "10000000"
    },
    "left_grip_selector": ".leftGrip",
    "right_grip_selector": ".rightGrip",
    "value_bar_selector": ".bar",
    "value_changed_callback": function (cause, leftValue, rightValue) {
        $('.leftLabel').val(leftValue);
        $('.rightLabel').val(rightValue);
    }
});

var site_banner = {};
site_banner.createCookie = function (name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
};
site_banner.readCookie = function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
};
site_banner.show = function(){ $("#site_banner").fadeIn(); };
site_banner.hide = function(){ $("#site_banner").hide(); };
site_banner.init = function(){
    $(".backdrop").click(function(){
        site_banner.hide();
    });
    
    var d = new Date();
    if(site_banner.readCookie('site_banner') == null) {
        site_banner.show();
        site_banner.createCookie('site_banner', d.getTime(), 1);
    }
};
site_banner.init();
$(document).ready(function(){
    var ii = -1;
    while (true) {
        ii++;
        if($("#slide-countdown-"+ii).length == 0){
            break;
        }
        $('#slide-countdown-'+ii).owlCarousel({

            loop:true,
            margin:0,
            responsiveClass:false,
            nav:true,
            dots:false,
            autoplay:false,
            autoHeight:false,
            autoplayTimeout:8000,
            autoplaySpeed:1000,
            autoplayHoverPause:false,
            navText:false,
            responsive:{
                0:{
                    items:1,
                    margin:0
                },
                576:{
                    items:2,
                    margin:6
                },
                768:{
                    items:3,
                    margin:10
                },
                992:{
                    items:4,
                    margin:10
                },
                1200:{
                    items:5,
                    margin:10
                },
                1440:{
                    items:5,
                    margin:16
                }
            }
        });


        var arrTime = $('.c-tag-countdown');
        var countDownDates = [];
        for(i = 0; i < arrTime.length; i++){
            countDownDates[i] = new Date(arrTime[i].innerText).getTime(); 
        }
        // Update the count down every 1 second
        var x = setInterval(function() {
            // Get todays date and time
            var now = new Date().getTime();
            // Find the distance between now and the count down date
            for(i = 0; i < arrTime.length; i++){
                var distance = countDownDates[i] - now;
                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                var el = arrTime[i];
                $(el).html('<span class="day">'+days+'</span><span class="hour">' + hours + '</span><span class="minute">' + minutes + '</span><span class="second">' + seconds + '</span>');    
            }   
        }, 1000);
    }
    
});
