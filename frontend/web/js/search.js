app.controller('search', function ($scope, $uibModal, modal) {
	$scope.categoryName = "Danh mục";
	$scope.categoryId = 0;
	$scope.keyword = (typeof searchKeyword == 'undefined') ? null : searchKeyword;
	$scope.suggests = [];

	$scope.category = function (id, name) {
		$scope.categoryName = name;
		$scope.categoryId = id;
	};

	$scope.search = function () {
		console.log($scope.keyword);
		if ($scope.keyword == null) {
			return;
		}
		loading.show();
		if ($scope.categoryId == 0) {
			document.location = baseUrl + '/tim-kiem-' + utils.createAlias($scope.keyword);
		} else {
			document.location = baseUrl + '/tim-kiem-' + utils.createAlias($scope.keyword) + '/' + utils.createAlias($scope.categoryName) + '-c' + $scope.categoryId;
		}
	};


	$scope.enterSearch = function(e) {
		if (e.keyCode == 13) {
			$scope.search();
		}
	}

	$scope.checkSearch = function(index, evt, keyword) {
		if(evt.which === 1) {
			$scope.keyword = keyword;
			$scope.search();

		}
		$scope.suggest = false;
	};
});
