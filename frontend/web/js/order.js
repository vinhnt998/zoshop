app.controller('order', function ($scope, $uibModal, fileDialog, modal) {

    $scope.categories = [];
    $scope.orders = [];

    $scope.buyer = {
        contactName: '',
        contactPhone: '',
        contactEmail: '',
        address: '',
        note: '',
    };

    $scope.init = function () {
    };

    $scope.list = function () {
        var cart = localStorage.getItem("cart");
        if (cart) {
            $.ajax({
                url: '/product/listincart',
                data: {
                    Data: cart
                },
                method: 'POST',
                success: function (resp) {
                    if (resp.success) {
                        $scope.$apply(function () {
                            $scope.orders = resp.data;
                        });
                    } else {
                        $scope.$apply(function () {
                            $scope.errors = resp.errors;
                        });
                    }
                    loading.hide();
                }
            });
        }
    };
    $scope.list();

    $scope.countProductInCart = 0;
    $scope.addcart = function (id = 0) {
        $scope.countProductInCart = 0;
        if (id) {
            var cart = localStorage.getItem("cart");
            if (cart) {
                var arr = JSON.parse(cart);
                var check = true;
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].pid == id) {
                        check = false;
                        arr[i].quantity = Number(arr[i].quantity) + 1;
                    }
                    $scope.countProductInCart += arr[i].quantity;
                }
                if (check) {
                    arr = arr.concat([{ "pid": id, "quantity": 1 }]);
                    $scope.countProductInCart += 1;
                }
                localStorage.setItem("cart", JSON.stringify(arr));
            } else {
                $scope.countProductInCart = 1;
                localStorage.setItem("cart", JSON.stringify([{ "pid": id, "quantity": 1 }]));
            }
        }
        $('#cuccac').html($scope.countProductInCart);
    };


    $scope.removecart = function (id = 0) {
        $scope.countProductInCart = 0;
        if (id) {
            var cart = localStorage.getItem("cart");
            if (cart) {
                var arr = JSON.parse(cart);
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].pid == id) {
                        if (arr[i].quantity > 1) {
                            arr[i].quantity = Number(arr[i].quantity) - 1;
                        }
                    }
                    $scope.countProductInCart += arr[i].quantity;
                }
                localStorage.setItem("cart", JSON.stringify(arr));
            }
        }
        $('#cuccac').html($scope.countProductInCart);
    };

    $scope.getNumberProduct = function (id) {
        var cart = localStorage.getItem("cart");
        if (cart) {
            var arr = JSON.parse(cart);
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].pid == id) {
                    return arr[i].quantity;
                }
            }
        }
    };

    $scope.getPrice = function (p) {
        var cart = localStorage.getItem("cart");
        if (cart) {
            var arr = JSON.parse(cart);
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].pid == p.id) {
                    return arr[i].quantity * p.price;
                }
            }
        }
    };

    $scope.totalPrice = function () {
        var total = 0;
        var cart = localStorage.getItem("cart");
        if (cart) {
            var arr = JSON.parse(cart);
            for (var i = 0; i < arr.length; i++) {
                for (var j = 0; j < $scope.orders.length; j++) {
                    if (arr[i].pid == $scope.orders[j].id) {
                        total += arr[i].quantity * $scope.orders[j].price;
                        break;
                    }
                }

            }
        }
        return total;
    };



    $scope.removeInCart = function (id = 0) {
        if (id) {
            var cart = localStorage.getItem("cart");
            if (cart) {
                var arr = JSON.parse(cart);
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].pid == id) {
                        arr.splice(i, 1);
                        break;
                    }
                }
                localStorage.setItem("cart", JSON.stringify(arr));
                $scope.list();

                $scope.countProductInCart = 0;
                for (var j = 0; j < arr.length; j++) {
                    $scope.countProductInCart += arr[j].quantity;
                }
                $('#cuccac').html($scope.countProductInCart);

            }
        }

        $scope.countProductInCart = 0;
        for (var i = 0; i < arr.length; i++) {
            $scope.countProductInCart += arr[i].quantity;
        }
        $('#cuccac').html($scope.countProductInCart);
    };

    $scope.getNow = function () {
        var arr = localStorage.getItem("cart");
        if (arr) {
            arr = JSON.parse(arr);
            var cmn = 0;
            for (var i = 0; i < arr.length; i++) {
                cmn += arr[i].quantity;
            }
            $('#cuccac').html(cmn + '');
        } else {
            $('#cuccac').html('0');
        }

    };
    $scope.getNow();

    $scope.ok = function () {
        var arr = localStorage.getItem("cart");
        // if (!userId) {
        //     bootbox.alert("Bạn chưa đăng nhập!");
        // }
        // arr = JSON.parse(arr);
        if (arr && arr.length > 10) {
            loading.show();
            $.ajax({
                url: '/product/buy',
                method: 'POST',
                data: {
                    Order: $scope.buyer,
                    Cart: arr,
                },
                success: function (resp) {
                    if (resp.success) {
                        $scope.$apply(function () {
                            if (resp.message) {
                                localStorage.setItem("cart", "");
                                window.location.href = "/hoa-don/" + resp.orderId;
                            }
                        }); 
                    } else {
                        $scope.$apply(function () {
                            $scope.errors = resp.data;
                            $("html, body").animate({ scrollTop: 100 }, "slow");
                        });
                    }
                    if (resp.message) {
                        bootbox.alert(resp.message);
                    }
                    loading.hide();
                }
            });
        } else {
            bootbox.alert("Giỏ hàng không có sản phẩm nào!");
        }
    };
});