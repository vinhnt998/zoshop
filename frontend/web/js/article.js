app.controller('article', function ($scope) {
    $scope.categories = categories;
    $scope.category = category;
    $scope.article = article;
    $scope.articles = articles;
    $scope.categoryId = category.id;

    $scope.changeCat = function (id) {
        $scope.categoryId = id;
    };

    $scope.createUrl = function (a) {
        return 'http://badasa.com.vn/bai-viet/' + utils.createAlias(a.name) + '-' + a.id + '.html';
    };
});