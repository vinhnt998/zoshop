app.controller('user', function ($scope, modal, auth) {
    $scope.user;
    $scope.areas = [];
    $scope.provinces = [];
    $scope.districts = [];

    $.ajax({
        url: baseUrl + '/user/get',
        method: 'GET',
        success: function (resp) {
            if (resp.success) {
                $scope.$apply(function () {
                    $scope.user = resp.data;
                });
            }
        }
    });

    $scope.getLocations = function () {
        $.ajax({
            url: baseUrl + '/user/getlocations',
            method: 'GET',
            success: function (resp) {
                if (resp.success) {
                    $scope.$apply(function () {
                        $scope.areas = resp.data.areas;
                        $scope.provinces = resp.data.provinces;
                        $scope.districts = resp.data.districts;
                        $scope.areas.splice(0, 0, {id: 0, name: 'Chọn khu vực'});
                        $scope.provinces.splice(0, 0, {id: 0, name: 'Chọn tỉnh thành'});
                        $scope.districts.splice(0, 0, {id: 0, name: 'Chọn quận huyện'});
                    });
                }
            }
        });
    };

    $scope.getLocations();

    $scope.save = function () {
        loading.show();
        $.ajax({
            url: baseUrl + '/user/save',
            data: utils.buildFormData('Customer', $scope.user),
            method: 'POST',
            success: function (resp) {
                loading.hide();
                if (resp.success) {
                    modal.alert('Cập nhật thông tin thành công');
                } else {
                    $scope.$apply(function () {
                        $scope.errors = resp.data;
                    });
                }
            }
        });
    };

    $scope.changeProvince = function(){
        if($scope.provinceId != 0){
            angular.forEach($scope.districts, function(value, key) {
                if($scope.districts[key].id != 0 && $scope.user.provinceId != $scope.districts[key].provinceId)
                    $scope.districts[key].hidden = true;
                else
                    $scope.districts[key].hidden = false;
            });
            $scope.user.districtId = 0;
        }
    }

});

app.controller('user_order', function ($scope, $uibModal, $q, $timeout) {

    $scope.orders = [];
    $scope.count = [];
    $scope.oid = '';
    $scope.name = '';
    $scope.item = 10;
    $scope.totalPage = 0;
    $scope.page = 1;
    $scope.totalItem = 0;

    
    $scope.list = function (page) {
        if(page){
            $scope.page = page;
        }
        loading.show();
        url = baseUrl + '/user/listorder';

        $.ajax({
            url: url,
            data: {
                page: $scope.page,
                item: $scope.item,
                status: $scope.status,
                oid: $scope.oid,
                name: $scope.name
            },
            method: 'GET',
            success: function (resp) {
                if (resp.success) {
                    angular.forEach(resp.orders, function (v, k) {
                        angular.forEach(resp.orders[k].products, function (b, l) {
                            b.productData = JSON.parse(b.productData);
                        });
                    });
                    $scope.$apply(function () {
                        $scope.orders = resp.orders;
                        $scope.count = resp.count;
                        $scope.totalPage = resp.page_count;
                        $scope.totalItem = resp.page_item_count;
                        $scope.page =  resp.page;
                    });
                }
                loading.hide();
            }
        });
    };
    $scope.list();

    $scope.getStatusClass = function (st) {
        switch (st*1) {
            case 0:
            return 'box-warning';
            break;
            case 1:
            return 'box-info';
            break;
            case 2:
            return 'box-primary';
            break;
            case 3:
            return 'box-success';
            break;
            case 4:
            return 'box-danger';
            break;
            default:
            return 'box-default';
        }
    };

    $scope.btnShow = function (id) {
        $("#show-" + id).slideToggle("slow");
    };


    $scope.rate = function (oid,pid) {
        $uibModal.open({
            animation: true,
            templateUrl: 'rateModal',
            controller: 'rateModal',
            size: 'lg',
            resolve: {
                params: function () {
                    return {
                        oid: oid,
                        pid: pid,
                        list: $scope.list()
                    }
                }
            }
        });
    };

});

app.controller('rateModal', function ($scope, $uibModal, $uibModalInstance, fileDialog, modal, params) {
    $scope.oid = params.oid;
    $scope.pid = params.pid;
    $scope.rating = {
        orderId: $scope.oid,
        productId: $scope.pid,
        rating: 0,
        content: ''
    }
    $scope.ok = function () {

        $.ajax({
            url: '/user/rating',
            data: {
                Rating: $scope.rating
            },
            method: 'POST',
            success: function (resp) {
                if (resp.success) {
                    $scope.$apply(function () {
                        if (resp.message) {
                            params.list;
                            modal.alert(resp.message);
                            $scope.cancel();

                        }
                    });
                } else {
                    $scope.$apply(function () {
                        $scope.errors = resp.data;
                        if (resp.message) {
                            modal.alert(resp.message);
                        }
                    });
                }
                loading.hide();
            }
        });

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});



