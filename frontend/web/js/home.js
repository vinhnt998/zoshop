
app.controller('homecontroller', function ($scope, $rootScope, auth, $uibModal,localStorageService) {
    $scope.products = [];
    $scope.homeBoxes = [];

    $scope.getHomeBoxProduct = function (obj) {
        loading.show();
        $.ajax({
            url: baseUrl + '/site/getproduct',
            method: 'GET',
            data: {
                order: obj.order,
                categoryId: obj.categoryId,
                areaId: obj.areaId,
                isSpecialties: obj.isSpecialties
            },
            success: function (resp) {
                loading.hide();
                if (resp.success) {
                    $scope.$apply(function () {
                        $scope.products[obj.location] = resp.data;
                    });
                }
            }
        });
    }

    $scope.init = function(location = 0, areaId = 0, categoryId = 0, order = 'latest'){
        $scope.homeBoxes[location] = {
            location            : location,
            areaId              : areaId                != -1 ? areaId     : $scope.homeBoxes[location].areaId,
            categoryId          : categoryId            != -1 ? categoryId : $scope.homeBoxes[location].categoryId,
            order               : order                 != '' ? order      : $scope.homeBoxes[location].order,
            isSpecialties       : 0,

        };
        $scope.getHomeBoxProduct($scope.homeBoxes[location]);
    }

    $scope.specialties = function(location = 0, areaId = 0, categoryId = 0, order = 'latest', isSpecialties){
        $scope.homeBoxes[location] = {
            location            : location,
            areaId              : areaId                != -1 ? areaId          : $scope.homeBoxes[location].areaId,
            categoryId          : categoryId            != -1 ? categoryId      : $scope.homeBoxes[location].categoryId,
            order               : order                 != '' ? order           : $scope.homeBoxes[location].order,
            isSpecialties       : isSpecialties         != 0 ? isSpecialties    : $scope.homeBoxes[location].isSpecialties,
        };
        $scope.getHomeBoxProduct($scope.homeBoxes[location]);
    }

    $scope.getPercent = function(a,b){
        // console.log(parseInt(b));
        // console.log(b);
        var k = b/a*100;
        return Math.ceil(100-k)+'%';
    }

});