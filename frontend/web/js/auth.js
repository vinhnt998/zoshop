app.service('auth', function ($uibModal) {
    this.signin = function () {
        $uibModal.open({
            animation: true,
            templateUrl: 'signinModal',
            controller: 'signinModal',
            size: 'sm',
            resolve: {
                params: function () {
                    return {}
                }
            }
        });
    };

    this.signup = function () {
        $uibModal.open({
            animation: true,
            templateUrl: 'signupModal',
            controller: 'signupModal',
            resolve: {
                params: function () {
                    return {}
                }
            }
        });
    };

    this.forgot = function () {
        $uibModal.open({
            animation: true,
            templateUrl: 'forgotModal',
            controller: 'forgotModal',
            resolve: {
                params: function () {
                    return {}
                }
            }
        });
    };
});

app.controller('auth', function ($scope, $rootScope, auth, $uibModal,localStorageService) {
    $scope.total_items = 0;
    $scope.final = 0;
    $scope.user = null;
    $scope.signin = function () {
        auth.signin();
    };

    if (window.location.hash) {
        if (window.location.hash.substring(1) == 'signin') {
            $scope.signin();
        }
    }



    $scope.signup = function () {
        auth.signup();
    };


    $scope.getNow = function () {
        var arr = localStorage.getItem("cart");
        if (arr) {
            arr = JSON.parse(arr);
            var cmn = 0;
            for (var i = 0; i < arr.length; i++) {
                cmn += arr[i].quantity;
            }
            $('#cuccac').html(cmn + '');
        } else {
            $('#cuccac').html('0');
        }

    };
    

    $scope.signout = function () {
        loading.show();
        $.ajax({
            url: baseUrl + '/auth/signout',
            method: 'GET',
            success: function (resp) {
                if (resp.success) {
                    $scope.$apply(function () {
                        localStorageService.remove('userInfo');
                        $scope.user = null;
                        $scope.buyer = null;
                        document.location.reload();
                    });
                }
                loading.hide();
            }
        });
    };

    $scope.init = function () {
        $scope.getNow();
    };
});

app.controller('signinModal', function ($scope, $rootScope, $uibModal,$uibModalInstance, params, auth, modal) {

    $scope.ok = function () {
        loading.show();
        $.ajax({
            url: baseUrl + '/auth/signin',
            data: {email: $scope.email, password: $scope.password, remember: $scope.remember},
            method: 'POST',
            success: function (resp) {
                if (resp.success) {
                    $scope.$apply(function () {
                        $uibModalInstance.dismiss('cancel');  
                        document.location.reload();
                    });
                } else {
                    $scope.$apply(function () {
                        $scope.error = resp.message;
                    });
                }
                loading.hide();
            }
        });
    };

    $scope.signup = function () {
        $uibModalInstance.dismiss('cancel');
        auth.signup();
    };

    $scope.forgot = function () {
        $uibModalInstance.dismiss('cancel');
        auth.forgot();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

app.controller('forgotModal', function ($scope, $uibModalInstance, params, auth, modal) {

    $scope.ok = function () {
        loading.show();
        $.ajax({
            url: baseUrl + '/auth/forgot',
            data: {email: $scope.email},
            method: 'POST',
            success: function (resp) {
                if (resp.success) {
                    $uibModalInstance.dismiss('cancel');
                    modal.alert('Yêu cầu lấy lại mật khẩu thành công, mật khẩu mới đã được gửi tới email của bạn!');
                } else {
                    $scope.$apply(function () {
                        $scope.error = resp.message;
                    });
                }
                loading.hide();
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

app.controller('signupModal', function ($scope, $uibModal, $uibModalInstance, params, auth, modal) {
    $scope.user = {
        name: '',
        phone: '',
        email: '',
        password: '',
        repassword: '',
        address: '',
    };
    $scope.ok = function () {
        loading.show();
        $.ajax({
            url: baseUrl + '/auth/signup',
            data: utils.buildFormData('Customer', $scope.user),
            method: 'POST',
            success: function (resp) {
                if (resp.success) {
                    $scope.$apply(function () {
                        $uibModalInstance.dismiss('cancel');
                    });
                } else {
                    $scope.$apply(function () {
                        $scope.errors = resp.data;
                    });
                }
                if (resp.message) {
                        modal.alert(resp.message);
                    }
                loading.hide();
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});


app.controller('passwordModal', function ($scope, $uibModalInstance, params, auth, modal) {
    $scope.ok = function () {
        loading.show();
        $.ajax({
            url: baseUrl + '/auth/password',
            data: {old: $scope.old, password: $scope.password},
            method: 'POST',
            success: function (resp) {
                if (resp.success) {
                    $uibModalInstance.dismiss('cancel');
                    modal.alert('Đổi mật khẩu thành công!');
                } else {
                    $scope.$apply(function () {
                        $scope.error = resp.message;
                    });
                }
                loading.hide();
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
