app.controller('product', function ($scope, $uibModal, fileDialog, modal) {
   

    $scope.countProductInCart = 0;
    $scope.addcart = function (id = 0) {
        loading.show();
        $scope.countProductInCart = 0;
        if (id) {
            var cart = localStorage.getItem("cart");
            if (cart) {
                var arr = JSON.parse(cart);
                var check = true;
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].pid == id) {
                        check = false;
                        arr[i].quantity = Number(arr[i].quantity) + 1;
                    }
                    $scope.countProductInCart += arr[i].quantity;
                }
                if (check) {
                    arr = arr.concat([{ "pid": id, "quantity": 1 }]);
                    $scope.countProductInCart += 1;
                }
                localStorage.setItem("cart", JSON.stringify(arr));
            } else {
                $scope.countProductInCart = 1;
                localStorage.setItem("cart", JSON.stringify([{ "pid": id, "quantity": 1 }]));
            }
        }
        $('#cuccac').html($scope.countProductInCart);
        loading.hide();
    };

    $scope.getNow = function () {
        var arr = localStorage.getItem("cart");
         
        if (arr) {
            arr = JSON.parse(arr);
            var cmn = 0;
            for (var i = 0; i < arr.length; i++) {
                cmn += arr[i].quantity;
            }
            $('#cuccac').html(cmn+'');
        } else {
            $('#cuccac').html('0');
        }
        
    };
    $scope.getNow();


    $scope.buynow = function(id){
        loading.show();
        $scope.addcart(id);
        location.href = '/gio-hang';
    };

    $scope.dp = [];
    $scope.dc = [];

    $scope.getDetail = function () {
        $scope.dc = category_detail;
        $scope.dp = product_detail;
    };

    $scope.getSale = function (price,promotion) {
        if (promotion < price) {
            return false;
        }
        var k = (price / promotion) * 100;
        return parseInt(100 - k);
    };


    $scope.VotePage = 1;
    $scope.VoteItem = 10;
    $scope.VoteTotalPage = 0;
    $scope.votes = [];
    $scope.voteUsers = [];
    $scope.VoteTotal = 0;

    $scope.getVote = function () {
        $.ajax({
            url: '/service/product/getvote',
            data: {
                ProductId: product_detail.Id,
                VotePage: $scope.VotePage,
                VoteItem: $scope.VoteItem
            },
            method: 'GET',
            success: function (resp) {
                if (resp.success) {
                    $scope.$apply(function () {
                        $scope.votes = resp.data;
                        $scope.VotePage = resp.votePage;
                        $scope.VoteTotalPage = resp.voteTotalPage;
                        $scope.voteUsers = resp.users;
                        $scope.VoteTotal = resp.voteTotal;
                    });
                } else {
                    $scope.$apply(function () {
                       
                    });
                    
                }
            }
        });
    };
});
