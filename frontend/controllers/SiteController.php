<?php

namespace frontend\controllers;

use common\components\ImageClient;
use common\components\TextUtils;
use common\models\Etc;
use common\models\Homebox;
use common\models\Alias;
use common\models\Import;
use common\models\Keyword;
use common\models\Banner;
use common\models\Product;
use common\models\ProductImage;
use common\models\HotProduct;
use common\models\Subscriber;
use common\models\Merchant;
use common\models\CategorySuggest;
use common\models\MerchantHighlight;
use common\models\Smbox;
use frontend\components\Controller;
use common\components\VerifyEmail;
use Yii;
use yii\helpers\Url;
use common\models\Area;
use app\models\Homeboxv2;
use common\models\Category;
use yii\db\Expression;
use common\models\BoxPromotion;


class SiteController extends Controller
{
	public function actionIndex(){

		$this->view->title = 'Zoshop - ngon vô địch - rẻ vô địch - nhanh vô địch';
		$products = Product::find()->andWhere(['status' => 0])->orderBy('id desc')->limit(18)->all();
		$boxs = BoxPromotion::find()->andWhere(['status' => 1])->with(['product'])->orderBy('position')->all();

		return $this->render('index',
			[
				'products' => $products,
				'boxs' => $boxs,
			]
		);
	}

	public function actionUpload(){
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		header("Access-Control-Allow-Origin: *");
		// header("Access-Control-Allow-Credentials: true");
		if(!empty($_FILES['file'])){

			$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
			$detectedType = exif_imagetype($_FILES['file']['tmp_name']);
			if(!in_array($detectedType, $allowedTypes)){
				return ['success' => false, 'message' => 'Tệp tin tải lên phải là hình ảnh!'];
			}

			$file = $_FILES['file'];
			$duoi = explode('.', $file['name']);
			$duoi = end($duoi);
			$datetime = date('YmdHis');
			$path = 'assets/uploads/'.$datetime.'.jpg';
			move_uploaded_file($_FILES['file']['tmp_name'],$path);
			return ['success'=>true,'data'=>$path];
		}
	}

	 public function actionKeyword($keyword = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($keyword != null) {
            $keyword = strtolower($keyword);
            $alias = TextUtils::createAlias($keyword);
            $key = Keyword::findOne(['alias' => $alias]);
            if ($key == null) {
                $key = new Keyword();
                $key->alias = $alias;
                $key->keyword = TextUtils::createKeyword($keyword);
                $key->originKeyword = $keyword;
                $key->firstHit = microtime(true);
            }
            $key->hit++;
            $key->lastHit = microtime(true);
            $key->hps = $key->hit / ($key->lastHit - $key->firstHit);
            $key->save();
        }

        return true;
    }

    public function actionVerifyEmail($email = ''){
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	$vmail = new VerifyEmail();
    	return $vmail->echeck($email);
    }
}
