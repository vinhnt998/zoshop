<?php

use common\components\ImageClient;
use yii\helpers\Url;
use frontend\widgets\HomeProduct;
use frontend\widgets\ProductWidget;
use common\models\Area;
use common\models\Category;
use common\models\Urls;
use common\models\Banner;
use common\components\TextUtils;

$banner = Banner::find()->all();
?>

<div class="l-banner">
  <div class="container container-v2">
    <div class="l-banner-box">
      <div class="row">
        <div class="col-sm-3 l-banner-box__left">
          <a href="<?= $banner[0]->url ?>"><img src="<?= $banner[0]->image ?>"></a>
        </div>
        <div class="col-sm-6 l-banner-box__center">
          <div class="col-sm-12">
            <a href="<?= $banner[1]->image ?>"><img src="<?= $banner[1]->image ?>"></a>
          </div>
          <div class="l-banner-box__center__row">
            <div class="col-sm-6">
              <a href="<?= $banner[3]->image ?>"><img src="<?= $banner[3]->image ?>"></a>
            </div>
            <div class="col-sm-6">
              <a href="<?= $banner[4]->image ?>"><img src="<?= $banner[4]->image ?>"></a>
            </div>
          </div>
        </div>
        <div class="col-sm-3 l-banner-box__right">
          <a href="<?= $banner[2]->image ?>"><img src="<?= $banner[2]->image ?>"></a>
        </div>
      </div>
    </div><!-- l-banner-box -->
  </div><!-- container -->
</div><!-- l-banner -->
<div class="c-commit-block">
  <div class="container container-v2">
    <div class="c-commit-box is-3-col">
      <div class="c-commit-box__item">
        <div class="c-commit-box__item__inner">
          <div class="c-commit-box__item__inner__img">
            <img src="static/images/v2/icon-commit-1.png">
          </div>
          <div class="c-commit-box__item__inner__content">
            <h2>NGON VÔ ĐỊCH</h2>
            <p>Độ ngon thách thức người khó tính nhất</p>
          </div>
        </div>
      </div><!-- c-commit-box__item -->
      <div class="c-commit-box__item">
        <div class="c-commit-box__item__inner">
          <div class="c-commit-box__item__inner__img">
            <img src="static/images/v2/icon-commit-2.png">
          </div>
          <div class="c-commit-box__item__inner__content">
            <h2>RẺ VÔ ĐỊCH</h2>
            <p>Đồ ăn có mức giá rất rẻ</p>
          </div>
        </div>
      </div><!-- c-commit-box__item -->
      <div class="c-commit-box__item">
        <div class="c-commit-box__item__inner">
          <div class="c-commit-box__item__inner__img">
            <img src="static/images/v2/icon-commit-3.png">
          </div>
          <div class="c-commit-box__item__inner__content">
            <h2>NHANH VÔ ĐỊCH</h2>
            <p>Đội ngũ giao hàng siêu nhanh</p>
          </div>
        </div>
      </div><!-- c-commit-box__item -->
    </div><!-- c-commit-box -->
  </div><!-- container -->
</div><!-- c-commit-block -->
<div class="container container-v2">
  <div class="c-box">
    <div class="c-box__title"><h2 class="c-box__title__name">Khuyến mại hấp dẫn</h2></div>
    <div class="c-box__content">
      <div class="c-template-slider is-sale">
        <div class="owl-carousel" id="slide-countdown-0">
          <?php foreach ($boxs as $box): ?>
            <div class="c-product-square">
              <div class="c-product-square__shadow">
                <div class="c-product-square__thumb">
                  <div class="c-tag-countdown"><?= date("M d, Y h:i:s", $box->endTime); ?></div>
                  <a href="<?= $box->product->createUrl() ?>">
                    <img src="/<?= $box->product->image ?>" alt="image" />
                  </a>
                </div>
                <div class="c-product-square__content">
                  <div class="c-product-square__row">
                    <a class="c-product-square__title" href="<?= $box->product->createUrl() ?>"><?= $box->product->name ?></a>
                  </div>
                  <div class="c-product-square__row">
                    <span class="c-product-square__price"><?= number_format($box->product->price) ?><sup>đ</sup></span>
                    <?php if($box->product->promotion > $box->product->price){ ?>
                    <span class="c-product-square__oldprice"><?= number_format($box->product->promotion) ?><sup>đ</sup></span>
                    <?php } ?>
                  </div>
                  <div class="c-product-square__row">
                    <span class="c-star is-<?= $box->product->rating ?>">
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                    </span>
                    <span class="c-product-square__view"><i class="fa fa-eye"></i><?= $box->product->view ?></span>
                  </div>
                  <div class="c-product-square__btn"><a href="<?= $box->product->createUrl() ?>">Mua ngay</a></div>
                </div>
              </div>
            </div>
          <?php endforeach ?>
          

        </div><!-- owl-carousel -->
      </div><!-- c-template-slider -->
    </div><!-- c-box__content -->
  </div><!-- c-box -->
  <div class="c-box">
    <div class="c-box__title"><h2 class="c-box__title__name">Menu Gọi món</h2></div>
    <div class="c-box__content">
      <div class="c-tabs clearfix">
        <div class="c-tabs__content">
          <div class="c-loading-gif" style="display: none;"></div>
          <div class="c-tabs__pane active" id="sach-tab-1" role="tabpanel">
            <div class="c-home-box is-big">
              <div class="c-home-product c-product-hover">
                <ul class="clearfix">
                  <?php foreach ($products as $product) { ?>
                  <li>
                    <div class="c-product-square">
                      <div class="c-product-square__shadow">
                        <div class="c-product-square__thumb">
                          <?php if($product->promotion && $product->promotion > $product->price){ 
                            $percent = floor(100-($product->price/$product->promotion*100));
                            ?>
                            <span class="c-tag-sale"><b>Giảm</b><?=$percent?>%</span>
                            <?php } ?>
                          <a href="<?= $product->createUrl() ?>"><img src="/<?= $product->image ?>" alt="image" /></a>
                        </div>
                        <div class="c-product-square__content">
                          <div class="c-product-square__row">
                            <a class="c-product-square__title" href="<?= $product->createUrl() ?>"><span class="icon-mall"></span><?= $product->name ?></a>
                          </div>
                          <div class="c-product-square__row">
                            <span class="c-product-square__price"><?= number_format($product->price) ?><sup>đ</sup></span>
                            <?php if($product->promotion > $product->price){ ?>
                            <span class="c-product-square__oldprice"><?= number_format($product->promotion) ?><sup>đ</sup></span>
                            <?php } ?>
                          </div>
                          <div class="c-product-square__row">
                            <span class="c-star is-<?= $product->rating ?>">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                            </span>
                            <span class="c-product-square__view"><i class="fa fa-eye"></i><?= $product->view ?></span>
                          </div>
                          <div class="c-product-square__btn"><a href="<?= $product->createUrl() ?>">Mua ngay</a></div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <?php } ?>
                </ul>
              </div><!-- c-home-product -->
            </div><!-- c-home-box -->
            <div class="c-more"><a class="btn btn-primary" href="/tim-kiem">Xem thêm</a></div>
          </div><!-- c-tabs__pane -->
        </div><!-- c-tabs__content -->
      </div><!-- c-tabs -->
    </div><!-- c-box__content -->
  </div><!-- c-box -->
</div><!-- container -->