<?php
use yii\helpers\Url;
use common\models\ProductSearch;
use common\models\Keyword;
use common\components\ImageClient;
use frontend\widgets\HomeProduct;

?>

<div class="container container-v2" ng-controller="search" ng-cloak>
	<div class="l-content">
		<div class="clearfix">
			<div class="l-main">
				<div class="c-browse-control">
					<div class="c-browse-control__title">

						<?php if($category){ ?>
						<h1>Danh mục: <?= $category->name ?></h1>
						<?php } ?>
						<?php if($keyword){ ?>
						<h1>Tìm kiếm: <?= $keyword ?></h1>
						<?php } ?>
					</div>
					<div class="c-browse-control__order">
						<form action="<?= explode('?', $_SERVER['REQUEST_URI'])[0] ?>">
							<label>Sắp xếp theo</label>
							<input type="hidden" name="cid" value="<?= $cid ?>">
							<select class="form-control" name="sort" onchange="this.form.submit()">
								<option value="0">Chọn</option>
								<option <?= $sort=='moi-nhat'?'selected':'' ?> value="moi-nhat">Mới nhất</option>
								<option <?= $sort=='thap-nhat'?'selected':'' ?> value="thap-nhat">Giá từ thấp đến cao</option>
								<option <?= $sort=='cao-nhat'?'selected':'' ?> value="cao-nhat">Giá từ cao xuống thấp</option>
								<option <?= $sort=='xem-nhieu'?'selected':'' ?> value="xem-nhieu">Xem nhiều nhất</option>
							</select>
						</form>
					</div>
				</div><!-- c-browse-control -->
				<div class="c-filter-mobile">
					<ul class="clearfix">
						<li><a href="#filter-category-id"><i class="fa fa-list-ul"></i>Danh mục</a></li>
						<li><a href="#filter-properties-id"><i class="fa fa-filter"></i>Lọc</a></li>
					</ul>
				</div><!-- c-filter-mobile -->
				<div class="c-product-grid c-product-hover">
					<?php if(is_array($products) && count($products) > 0){ ?>
					<ul class="clearfix">
						<?php foreach ($products as $product): ?>
							<li>
								<div class="c-product-square">
									<div class="c-product-square__shadow">
										<div class="c-product-square__thumb">
											<?php if($product->promotion && $product->promotion > $product->price){ 
												$percent = floor(100-($product->price/$product->promotion*100));
												?>
												<span class="c-tag-sale"><b>Giảm</b><?=$percent?>%</span>
												<?php } ?>
												<a href="<?= $product->createUrl() ?>">
													<img src="/<?= $product->image ?>" alt="<?= $product->name ?>">
												</a>
											</div>
											<div class="c-product-square__content">
												<div class="c-product-square__row">
													<a class="c-product-square__title" href="<?= $product->createUrl() ?>"><?= $product->name ?></a>
												</div>
												<div class="c-product-square__row">
													<span class="c-product-square__price"><?= number_format($product->price) ?><sup>đ</sup></span>
													<?php if($product->promotion > $product->price){ ?>
													<span class="c-product-square__oldprice"><?= number_format($product->promotion) ?><sup>đ</sup></span>
													<?php } ?>
												</div>
												<div class="c-product-square__row">
													<span class="c-star is-<?= $product->rating ?>">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</span> 
													<span class="c-product-square__view"><i class="fa fa-eye"></i><?= $product->view ?></span>
												</div>
												<div class="c-product-square__btn"><a href="<?= $product->createUrl() ?>">Mua ngay</a></div>
											</div>
										</div>
									</div>
								</li>
							<?php endforeach ?>
						</ul>
					<?php }else{ ?>
					<ul class="clearfix">
						<li>Không có sản phẩm nào!</li>
					</ul>
					<?php } ?>
					</div><!-- c-product-grid -->
					<?php if(is_array($products) && count($products) > 0){ ?>
					<div class="b-page">
						<div class="pagination-router">
							<ul class="pagination">
								<?php if($page - 1 > 0 ){ ?>
								<li><a href="<?= explode('?', $_SERVER['REQUEST_URI'])[0] ?>?cid=<?= $cid ?>&sort=<?= $sort ?>&page=<?= $page - 1 ?>">&lt;</a></li>
								<?php } ?>
								<?php if($page - 2 > 0 ){ ?>
								<li><a href="<?= explode('?', $_SERVER['REQUEST_URI'])[0] ?>?cid=<?= $cid ?>&sort=<?= $sort ?>&page=<?= $page - 2 ?>"><?= $page - 2 ?></a></li>
								<?php } ?>
								<?php if($page - 1 > 0 ){ ?>
								<li><a href="<?= explode('?', $_SERVER['REQUEST_URI'])[0] ?>?cid=<?= $cid ?>&sort=<?= $sort ?>&page=<?= $page - 1 ?>"><?= $page - 1 ?></a></li>
								<?php } ?>
								<li class="active"><a><?= $page ?></a></li>
								<?php if($page + 1 <= $page_count ){ ?>
								<li><a href="<?= explode('?', $_SERVER['REQUEST_URI'])[0] ?>?cid=<?= $cid ?>&sort=<?= $sort ?>&page=<?= $page + 1 ?>"><?= $page + 1 ?></a></li>
								<?php } ?>
								<?php if($page + 2 <= $page_count  ){ ?>
								<li><a href="<?= explode('?', $_SERVER['REQUEST_URI'])[0] ?>?cid=<?= $cid ?>&sort=<?= $sort ?>&page=<?= $page + 2 ?>"><?= $page + 2 ?></a></li>
								<?php } ?>
								<?php if($page + 1 <= $page_count ){ ?>
								<li><a href="<?= explode('?', $_SERVER['REQUEST_URI'])[0] ?>?cid=<?= $cid ?>&sort=<?= $sort ?>&page=<?= $page + 1 ?>">&gt;</a></li>
								<?php } ?>
							</ul>
							</div><!-- pagination-router -->
						</div>
					<?php } ?>
					</div><!-- l-main -->
					<div class="l-sidebar">
						<div class="c-sidebar-ovelay js-sidebar-ovelay"></div>
						<div class="c-sidebar-filter" id="filter-category-id">
							<span class="c-sidebar-close js-sidebar-close"><i class="fa fa-long-arrow-left"></i></span>
							<div class="c-whitebox">
								<div class="c-whitebox__title">
									<a class="c-whitebox__title__name" href="/tim-kiem">Danh mục</a>
								</div>
								<div class="c-whitebox__content">
									<div class="c-sidebar-menu">
										<ul>
											<?php foreach ($categories as $cats): ?>
												<li class="">
													<a href="<?= $cats->createUrl() ?>"><?= $cats->name ?><span>(<?= $cats->getCount() ?>)</span></a>
												</li>
											<?php endforeach ?>
										</ul>
									</div><!-- c-sidebar-menu -->
								</div>
							</div><!-- c-whitebox -->
						</div><!-- c-sidebar-filter -->
					</div><!-- l-sidebar -->
				</div><!-- clearfix -->
			</div><!-- l-content -->
      </div><!-- container -->