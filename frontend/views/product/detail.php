<?php
use common\components\ImageClient;
use common\models\ProductSearch;
use common\models\ProductUnit;
use yii\helpers\Url;
use frontend\widgets\ProductWidget;
use common\components\TextUtils;
use yii\widgets\LinkPager;

?>
<div class="container container-v2" ng-controller="product" ng-init="init()">
	<div class="l-content">
		<ol class="breadcrumb">
			<li><a href="<?= Url::home() ?>">Trang chủ</a></li>
			<li><a href="<?= $product->category->createUrl() ?>"><?= $product->category->name ?></a></li>
			<li><a href="<?= $product->createUrl() ?>"><?= $product->name ?></a></li>
		</ol>
		<div class="c-product">
			<div class="c-product-head">
				<div class="c-product__row">
					<h1 class="c-product-head__title"><?= $product->name ?></h1>
				</div>
				<div class="c-product__row">
					<h2 class="c-product-head__desc">
					</h2>
				</div>
			</div><!-- c-product-head -->
			<div class="clearfix">
				<div class="c-product-left">
					<div class="c-product-image">
						<?php if($product->promotion && $product->promotion > $product->price){  $percent = floor(100-($product->price/$product->promotion*100)); ?>
						<span class="c-tag-sale"><b>Giảm</b><?=$percent?>%</span>
						<?php } ?>
						<div class="b-table">
							<div class="b-table__row">
								<div class="b-table__cell">
									<img id="myCloudZoom" class="cloudzoom" src="/<?= $product->image ?>" data-cloudzoom ="zoomSizeMode: 'image',zoomPosition:'inside',zoomOffsetX:0, zoomImage: '/<?= $product->image ?>'" alt="<?= $product->name ?>" />
								</div>
							</div>
						</div>
					</div><!-- c-product-image -->
					<div class="c-product-social">
						<ul>
							<li><label>Chia sẻ:</label></li>
							<li><a class="bg-facebook" href="#"><i class="fa fa-facebook-f"></i></a></li>
							<li><a class="bg-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a class="bg-instagram" href="#"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div><!-- c-product-social -->
				</div><!-- c-product-left -->
				<div class="c-product-main">
					<div class="c-product-main__gadient">
						<div class="c-product__row">
							<label>Mã sản phẩm</label>
							<div class="c-product-main__text"><span class="text-primary"><?= $product->id ?></span></div>
						</div>
						<div class="c-product__row">
							<label class="c-product-main__lb">Giá bán</label>
							<div class="c-product-main__text">                
								<span class="c-product-main__price"><?= number_format($product->price) ?><sup>đ</sup></span>
							</div>
						</div>
						<?php if($product->promotion && $product->promotion > $product->price){ ?>
						<div class="c-product__row">
							<label>Giá ban đầu</label>
							<div class="c-product-main__text">
								<span class="c-product-main__oldprice"><?= number_format($product->promotion) ?><sup>đ</sup></span>
							</div>
						</div>
						<?php } ?>
						<div class="c-product__row">
							<span class="c-star is-<?=$product->rating ?>">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</span> 
							(<?= count($product->ratings) ?> lượt đánh giá)
						</div>
					</div><!-- detail-center-gadient -->
					<div class="c-product__line"></div>
					<div class="c-product__row">
						<div class="c-product-main__support">
							<?= $product->description ?>
						</div>
					</div>

					<div class="c-product__row c-product-main__button">
						<?php if($product->status == 0){ ?>
						<a class="btn btn-primary btn-lg text-uppercase" href="javascript:;" ng-click="buynow(<?= $product->id ?>)">Mua luôn</a>
						<a class="btn btn-secondary btn-lg text-uppercase" href="javascript:;" ng-click="addcart(<?= $product->id ?>)">Thêm vào giỏ</a>
						<?php } else { ?>
						<a class="btn btn-danger btn-lg text-uppercase" href="javascript:;">Hết hàng</a>
						<?php }?>
					</div>

					<div class="c-product__row">
						<div class="c-product-main__support">
							<p class="c-product__check"><i class="fa fa-volume-control-phone"></i><?=Yii::$app->controller->contactPhone?> | Email: <?=Yii::$app->controller->contactEmail?></p>
							<p class="c-product__check"><i class="fa fa-calendar-o"></i> 08h - 22h tất cả các ngày trong tuần</p>
						</div><!-- c-product-main__support -->
					</div>
				</div><!-- c-product-main -->
				<div class="c-product-right">
					<div class="c-product-right__gadient">
						<div class="c-product-right__commit">
							<ul>
								<li>
									<div class="b-grid">
										<div class="b-grid__img"><img src="/static/images/detail-commit_1.png" alt="commit 1" /></div>
										<div class="b-grid__content">
											<div class="b-grid__row"><span class="b-grid__title">SẢN PHẨM ĐA DẠNG</span></div>
											<div class="b-grid__row">Vô vàn đặc sản từ khắp mọi miền tổ quốc với vô số ngành hàng</div>
										</div>
									</div><!-- b-grid -->
								</li>
								<li>
									<div class="b-grid">
										<div class="b-grid__img"><img src="/static/images/detail-commit_2.png" alt="commit 2" /></div>
										<div class="b-grid__content">
											<div class="b-grid__row"><span class="b-grid__title">ĐẢM BẢO CHÍNH HÃNG</span></div>
											<div class="b-grid__row">Sản phẩm có nguồn gốc rõ ràng, có chứng nhận VSATTP.</div>
										</div>
									</div><!-- b-grid -->
								</li>
								<li>
									<div class="b-grid">
										<div class="b-grid__img"><img src="/static/images/detail-commit_3.png" alt="commit 3" /></div>
										<div class="b-grid__content">
											<div class="b-grid__row"><span class="b-grid__title">GIÁ LUÔN LUÔN TỐT</span></div>
											<div class="b-grid__row">Mua hàng với giá vô cùng cạnh tranh, tiết kiệm thời gian và chi phí.</div>
										</div>
									</div><!-- b-grid -->
								</li>
								<li>
									<div class="b-grid">
										<div class="b-grid__img"><img src="<?= Url::base() ?>/static/images/detail-commit_4.png" alt="commit 4" /></div>
										<div class="b-grid__content">
											<div class="b-grid__row"><span class="b-grid__title">VẬN CHUYỂN CHUYÊN NGHIỆP</span></div>
											<div class="b-grid__row">Giao hàng nhanh chóng và thân thiện</div>
										</div>
									</div><!-- b-grid -->
								</li>
							</ul>
						</div><!-- c-product-right__commit -->
					</div><!-- c-product-right__gadient -->
				</div><!-- c-product-right -->
			</div><!-- clearfix -->
		</div><!-- c-product -->
		<div class="clearfix">
			<div class="c-product-content">
				<?php /* ?>
				<div class="c-tabs is-detail">
					<div class="c-tabs__title">
						<ul>
							<li class="active"><a href="#tab1">Thông tin sản phẩm</a></li>
						</ul>
					</div><!-- c-tabs__title -->
					<div class="c-tabs__container">
						<div class="c-tabs__pane active" id="tab1">
							<div class="b-maincontent">
								<?= $product->description ?>
							</div>
						</div><!-- c-tabs__pane -->
					</div><!-- c-tabs__container -->
				</div><!-- c-tabs -->
				*/?>
				<div class="c-tabs is-detail">
					<div class="c-tabs__title">
						<ul>
							<li class="active"><a href="#tab1">Đánh giá & Bình luận</a></li>
						</ul>
					</div><!-- c-tabs__title -->
					<div class="c-tabs__container">
						<div class="c-tabs__pane active" id="tab1">
							<div class="c-rate-list">
								<?php foreach ($product->ratings as $key => $com) { ?>
								<div class="c-rate-item">
									<div class="c-rate-item__row">
										<span class="c-star is-<?=$com->rating?>"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
										<span class="c-rate-item__title"></span>
									</div>
									<div class="c-rate-item__row"><span class="c-rate-item__time">Bởi  lúc <?=date('d/m/Y H:i',$com->createTime )?></span></div>
									<div class="c-rate-item__row c-rate-item__content">
										<?=$com->content ?>               
									</div>
								</div><!-- c-rate-item -->
								<?php } ?>
								<?php if(!isset($product->ratings) || count($product->ratings) == 0){ ?>
								<div class="c-rate-item">
									<div class="c-rate-item__row c-rate-item__content">
										Chưa có đánh giá!           
									</div>
								</div><!-- c-rate-item -->
								<?php } ?>
							</div><!-- c-rate-list -->

						</div><!-- c-tabs__pane -->
					</div><!-- c-tabs__container -->
				</div><!-- c-tabs -->

			</div><!-- c-product-content -->
		</div><!-- clearfix -->
	</div><!-- l-content -->
</div><!-- container -->