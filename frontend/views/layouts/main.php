<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\Navigation;
use frontend\widgets\Footer;
use frontend\widgets\Modal;
use frontend\assets\AppAsset;
use yii\helpers\Json;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta http-equiv="content-language" content="vi"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta property="fb:app_id" content="650946688680737"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <?= Html::csrfMetaTags() ?>
        <link rel="shortcut icon" href="<?= Url::base() ?>/static/images/favicon.ico" />
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body ng-app="app" class="bg-main" ng-cloak>
    
    <?= Navigation::widget() ?>
    <?= $content ?>
    <?= Footer::widget() ?>
    <?= Modal::widget() ?>
    <script>
        var baseUrl = '<?= Url::base() ?>';
    </script>
    <?php $this->endBody(); ?>
    <script>
    CloudZoom.quickStart();
    $('.nstSlider').nstSlider({
        "rounding": {
        "1000000" : "10000000"
        },
        "left_grip_selector": ".leftGrip",
        "right_grip_selector": ".rightGrip",
        "value_bar_selector": ".bar",
        "value_changed_callback": function(cause, leftValue, rightValue) {
        $('.leftLabel').val(leftValue);
        $('.rightLabel').val(rightValue);
        }
    });
    </script>
    <script>
        $(document).ready(function () {
            <?= Yii::$app->controller->clientScript ?>;
        });
    </script>
    <div id="fb-root"></div>
    </body>
    </html>
<?php $this->endPage(); ?>