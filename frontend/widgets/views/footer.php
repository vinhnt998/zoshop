<?php
use yii\helpers\Url;
use common\components\ImageClient;
use common\models\Keyword;
?>
<div class="l-footer">
  <div class="l-footer__commit">
    <div class="container container-v2">
      <div class="l-footer__commit__item">
        <div class="l-footer__commit__item__inner">
          <div class="l-footer__commit__item__inner__img">
            <img src="static/images/v2/ico-footer-commit-1.png">
          </div>
          <div class="l-footer__commit__item__inner__content">
            <h2>Miễn phí vận chuyển</h2>
            <p>Miễn phí ship dưới 20km</p>
          </div>
        </div>
      </div>
      <div class="l-footer__commit__item">
        <div class="l-footer__commit__item__inner">
          <div class="l-footer__commit__item__inner__img">
            <img src="static/images/v2/ico-footer-commit-2.png">
          </div>
          <div class="l-footer__commit__item__inner__content">
            <h2>Bảo mật thông tin KH</h2>
            <p>Miễn phí ship dưới 20km</p>
          </div>
        </div>
      </div>
      <div class="l-footer__commit__item">
        <div class="l-footer__commit__item__inner">
          <div class="l-footer__commit__item__inner__img">
            <img src="static/images/v2/ico-footer-commit-3.png">
          </div>
          <div class="l-footer__commit__item__inner__content">
            <h2>Tư Vấn Online</h2>
            <p>Miễn phí ship dưới 20km</p>
          </div>
        </div>
      </div>
      <div class="l-footer__commit__item">
        <div class="l-footer__commit__item__inner">
          <div class="l-footer__commit__item__inner__img">
            <img src="static/images/v2/ico-footer-commit-4.png">
          </div>
          <div class="l-footer__commit__item__inner__content">
            <h2>Thanh toán tiện lợi</h2>
            <p>Miễn phí ship dưới 20km</p>
          </div>
        </div>
      </div>
    </div><!-- container -->
  </div><!-- l-footer__commit -->
  <div class="l-footer__email">
    <div class="container container-v2">
      <div class="row">
        <div class="col-sm-6 l-footer__email__left">
          <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
          <span>ĐĂNG KÝ NGAY HÔM NAY NHẬN VOUCHER KHUYẾN MÃI LÊN ĐẾN 50%</span>
        </div>
        <div class="col-sm-6 l-footer__email__right">
          <div class="form-group">
            <input type="email" class="form-control" placeholder="Địa chỉ Email của bạn">
          </div>
          <button type="submit" class="btn"><i class="fa fa-envelope" aria-hidden="true"></i></button>
        </div>
      </div>
    </div>
  </div><!-- l-footer__email -->
  <div class="l-footer__main">
    <div class="container container-v2">
      <div class="row">
        <div class="col-sm-4">
          <div class="c-footer-logo"><a href="#"><img src="static/images/zoshop-2.png" alt="logo" /></a></div>
          <div class="c-footer-text">
            <h6><b><?=Yii::$app->controller->slogan?></b></h6>
            <p>Địa chỉ: <?=Yii::$app->controller->address?></p>
            <p>Điện thoại: <?=Yii::$app->controller->contactPhone?></p>
            <p>Email: <?=Yii::$app->controller->contactEmail?></p>
          </div>
          <div class="c-footer-social clearfix">
            <h2>Kết nối</h2>
            <div class="c-footer-social__icon">
              <a href="#"><i class="fa fa-facebook"></i></a>
              <a href="#"><i class="fa fa-instagram"></i></a>
              <a href="#"><i class="fa fa-youtube-play"></i></a>
              <a href="#"><i class="icon32-zalo"></i></a>
            </div>
          </div>
        </div><!-- col -->
        <div class="col-sm-2">
          <div class="c-footer-label">HỖ TRỢ<i class="fa fa-plus"></i></div>
          <div class="c-footer-content">
            <ul class="c-footer-list">
              <li><a href="#">Trung tâm trợ giúp</a></li>
              <li><a href="#">Hướng dẫn mua hàng</a></li>
              <li><a href="#">Vận chuyển</a></li>
              <li><a href="#">Trả hàng và hoàn tiền</a></li>
            </ul>
          </div><!-- c-footer-content -->
        </div><!-- col -->
        <div class="col-sm-2">
          <div class="c-footer-label">Về ZOSHOP<i class="fa fa-plus"></i></div>
          <div class="c-footer-content">
            <ul class="c-footer-list">
              <li><a href="#">Giới Thiệu</a></li>
              <li><a href="#">Tuyển dụng</a></li>
              <li><a href="#">Điều khoản</a></li>
              <li><a href="#">Chính sách bảo mật</a></li>
            </ul>
          </div><!-- c-footer-content -->
        </div><!-- col -->
        <div class="col-sm-4">
          <div class="c-footer-label">bản tin KHUYẾN MÃI<i class="fa fa-plus"></i></div>
          <div class="c-footer-new c-footer-content">
            <div class="c-footer-new__item">
              <div class="c-footer-new__item__date">
                <label>25</label>
                <p>T - 11</p>
              </div><!-- c-footer-new__item__date -->
              <div class="c-footer-new__item__content">
                <h3><a href="#">Khuyến mãi chào mùa hè</a></h3>
                <p>Chương trình diễn ra từ ngày 20 tháng 10 đến 5 tháng 12 2019 cho mọi lứa tuổi...</p>
              </div><!-- c-footer-new__item__content -->
            </div>
            <div class="c-footer-new__item">
              <div class="c-footer-new__item__date">
                <label>25</label>
                <p>T - 11</p>
              </div><!-- c-footer-new__item__date -->
              <div class="c-footer-new__item__content">
                <h3><a href="#">Khuyến mãi chào mùa đông</a></h3>
                <p>Chương trình diễn ra từ ngày 20 tháng 10 đến 5 tháng 12 2019 cho mọi lứa tuổi...</p>
              </div><!-- c-footer-new__item__content -->
            </div>
          </div><!-- c-footer-new -->
        </div><!-- col -->
      </div><!-- row -->
    </div>
  </div><!-- l-footer__main -->
  <div class="l-footer__bottom">
    <div class="container container-v2">
      <div class="row">
        <div class="col-sm-4 l-footer__bottom__visa">
          <img src="static/images/v2/visa.png">
        </div>
        <div class="col-sm-4 l-footer__bottom__center">
          <img src="static/images/v2/bo-cong-thuong-2.png">
        </div>
        <div class="col-sm-4 l-footer__bottom__down">
          <a href="#">
            <img src="static/images/v2/download-google.png">
          </a>
          <a href="#">
            <img src="static/images/v2/download-store.png">
          </a>
          <a href="#">
            <img src="static/images/v2/download-store-2.png">
          </a>
        </div>
      </div>
    </div>
  </div><!-- l-footer__bottom -->
  <div class="l-footer__tag">
    <div class="container container-v2">
      <ul class="clearfix">
        <li><a href="#">Đồ ăn ngon</a></li>
        <li><a href="#">Đồ ăn rẻ</a></li>
        <li><a href="#">Giao hàng nhanh</a></li>
      </ul>
    </div>
  </div>
  <div class="l-footer__copyright">
    <div class="container container-v2">
      <p>©2019 ZOSHOP. All Rights Reserved</p>
    </div>
  </div>
</div><!-- l-footer -->