<?php
use yii\helpers\Url;

?>
<script type="text/ng-template" id="signinModal">
	<div class="modal-header">
		<button type="button" class="close" ng-click="cancel()" aria-label="Close"><i class="fa fa-times-circle"></i></button>
	</div>
	<div class="modal-body" ng-cloak>
		<div class="content-login">
			<div class="info-content-login form-group">
				<h3>Đăng nhập</h3>
				<form ng-submit="ok()">
					<div class="form-group">
						<label>Email hoặc Số điện thoại</label>
						<input ng-model="email" type="text" class="form-control" placeholder="Email hoặc Số điện thoại">
					</div>
					<div class="form-group">
						<label>Mật khẩu</label>
						<input ng-model="password" type="password" class="form-control" placeholder="Mật khẩu của bạn">
					</div>
					<div class="form-group">
						<label><input ng-model="remember" type="checkbox" value=""> Tự đăng nhập lần sau</label>
					</div><!-- form-group -->
					<div class="alert alert-danger" ng-if="error!=null">{{error}}</div>
					<div class="form-group">
						<input type="submit" value="Đăng nhập" class="btn btn-secondary btn-lg"/>
						<span class="pull-right link-regiter"><a ng-click="signup()" style="cursor: pointer">Đăng ký tài khoản</a>&nbsp;|&nbsp;<a ng-click="forgot()" style="cursor: pointer">Quên mật khẩu</a></span>
					</div>
				</form>
			</div>
		</div><!-- end .info-content-modal-->
		<div class="clearfix"></div>
	</div><!-- modal-body -->
	<div class="modal-footer">
      <!-- <div class="info-footer text-left">
          <span>Đăng nhập với</span>
      </div> -->
  </div>
</script>

<script type="text/ng-template" id="passwordModal">
	<div class="modal-header">
		<button type="button" class="close" ng-click="cancel()"><i class="fa fa-times-circle"></i>
		</button>
	</div>
	<div class="modal-body" ng-cloak>
		<div class="content-login">
			<div class="info-content-login form-group">
				<h3>Đổi mật khẩu</h3>
				<form ng-submit="ok()">
					<div class="form-group">
						<label>Mật khẩu cũ</label>
						<input ng-model="old" type="password" class="form-control" placeholder="Mật khẩu cũ của bạn">
					</div>
					<div class="form-group">
						<label>Mật khẩu mới</label>
						<input ng-model="password" type="password" class="form-control" placeholder="Mật khẩu mới bạn muốn đổi">
					</div>
					<div class="alert alert-danger" ng-if="error!=null">{{error}}</div>
					<div class="form-group">
						<input type="submit" class="btn btn-secondary" value="Đổi mật khẩu"/>
					</div>
				</form>
			</div>
		</div><!-- end .info-content-modal-->
		<div class="clearfix"></div>
	</div><!-- modal-body -->
</script>

<script type="text/ng-template" id="signupModal">
	<div class="modal-header">
		<button type="button" class="close" ng-click="cancel()" aria-label="Close"><i class="fa fa-times-circle"></i></button>
	</div>
	<div class="modal-body" ng-cloak>
        <div class="c-account-user">
			<div class="c-account-user__title">Đăng ký</div>
			<div class="c-account-user__content">
				<form ng-submit="ok()" class="b-form">
					<div class="form-group">
						<h4><i class="fa fa-user"></i>Thông tin cá nhân</h4>
					</div>

					<div ng-class="errors.name?'has-error form-group':'form-group'">
						<label>Họ tên (<span class="red">*</span>)</label>
						<input ng-model="user.name" type="text" class="form-control" placeholder="Họ và tên"/>
						<p class="help-block" ng-if="errors.name">{{errors.name[0]}}</p>
					</div>
					<div ng-class="errors.phone?'has-error form-group':'form-group'">
						<label>Số điện thoại (<span class="red">*</span>)</label>
						<input ng-model="user.phone" type="text" class="form-control" placeholder="0123456789"/>
						<p class="help-block" ng-if="errors.phone">{{errors.phone[0]}}</p>
					</div>
					<div ng-class="errors.email?'has-error form-group':'form-group'">
						<label>Email</label>
						<input ng-model="user.email" type="text" class="form-control" placeholder="email"/>
						<p class="help-block" ng-if="errors.email">{{errors.email[0]}}</p>
					</div>
					<div ng-class="errors.password?'has-error form-group':'form-group'">
						<label>Mật khẩu (<span class="red">*</span>)</label>
						<input ng-model="user.password" type="password" class="form-control" placeholder="Chọn mật khẩu khó đoán"/>
						<p class="help-block" ng-if="errors.password">{{errors.password[0]}}</p>
					</div>
					<div ng-class="errors.repassword?'has-error form-group':'form-group'">
						<label>Nhập lại mật khẩu (<span class="red">*</span>)</label>
						<input ng-model="user.repassword" type="password" class="form-control" placeholder="Chọn mật khẩu khó đoán"/>
						<p class="help-block" ng-if="errors.repassword">{{errors.repassword[0]}}</p>
					</div>
					<div ng-class="errors.address?'has-error form-group':'form-group'">
						<label>Địa chỉ nhận hàng</label>
						<input ng-model="user.address" type="text" class="form-control" placeholder=""/>
						<p class="help-block" ng-if="errors.address">{{errors.address[0]}}</p>
					</div>
					<div class="form-group">
						<label>Bằng việc đăng ký bạn đã đồng ý với <a href="https://tintuc.voso.vn/quy-che-hoat-dong/"  target="_blank">Điều khoản thành viên</a> & <a href="https://tintuc.voso.vn/quy-che-hoat-dong/" target="_blank"> điều kiện giao dịch</a> của Zoshop</label>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-secondary" value="Đăng ký"/>
					</div>
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</div><!-- modal-body -->
</script>

<script type="text/ng-template" id="forgotModal">
	<div class="modal-header">
		<button type="button" class="close" ng-click="cancel()"><i class="fa fa-times-circle"></i>
		</button>
	</div>
	<div class="modal-body" ng-cloak>
		<div class="content-login">
			<div class="info-content-login form-group">
				<h3>Quên mật khẩu</h3>
				<form ng-submit="ok()">
					<div class="form-group">
						<label>Email đăng ký</label>
						<input ng-model="email" type="text" class="form-control" placeholder="Địa chỉ email của bạn">
					</div>
					<div class="alert alert-danger" ng-if="error!=null">{{error}}</div>
					<div class="form-group">
						<input type="submit" value="Lấy lại mật khẩu" class="btn btn-secondary btn-login-modal btn-green btn-login"/>
					</div>
				</form>
			</div>
		</div><!-- end .info-content-modal-->
		<div class="clearfix"></div>
	</div><!-- modal-body -->
	<div ng-if="0" class="modal-footer">
		<div class="info-footer text-left">
			<ul class="list-inline">
				<li>Đăng nhập với:</li>
				<li><a href="#"><span class="icon-fb"></span></a></li>
				<li><a href="#"><span class="icon-ya"></span></a></li>
				<li><a href="#"><span class="icon-gg"></span></a></li>
			</ul>
		</div>
	</div>
</script>

<script type="text/ng-template" id="alertModal">
	<div class="modal-header">
		<button type="button" class="close" ng-click="cancel()" aria-label="Close">
			<i class="fa fa-times-circle"></i>
		</button>
	</div>
	<div class="modal-body" ng-cloak>
		<div class="content-login">
			<div class="info-content-login form-group">
				<h3>Thông báo</h3>
				<p>{{message}}</p>
			</div>
		</div><!-- end .info-content-modal-->
		<div class="clearfix"></div>
	</div><!-- modal-body -->
	<div class="modal-footer">
		<div class="text-right">
			<button ng-click="cancel()" type="button" class="btn btn-secondary">Đồng ý</button>
		</div>
	</div>
</script>

<script type="text/ng-template" id="alertHtmlModal">
	<div class="modal-header">
		<button type="button" class="close" ng-click="cancel()" aria-label="Close">
			<i class="fa fa-times-circle"></i>
		</button>
	</div>
	<div class="modal-body" ng-cloak>
		<div class="content-login">
			<div class="info-content-login form-group">
				<h3>Thông báo</h3>
				<p ng-bind-html="message"></p>
			</div>
		</div><!-- end .info-content-modal-->
		<div class="clearfix"></div>
	</div><!-- modal-body -->
	<div class="modal-footer">
		<div class="text-right">
			<button ng-click="cancel()" type="button" class="btn btn-secondary">Đồng ý</button>
		</div>
	</div>
</script>

<script type="text/ng-template" id="rateModal">
	<div class="modal-header">
		<button type="button" class="close" ng-click="cancel()"><i class="fa fa-times-circle"></i>
		</button>
	</div>
	<div class="modal-body" ng-cloak>
				<div ng-class="errors.rating?'has-error form-group':'form-group'">
					<label>Đánh giá</label>
					<div class="form-control">
						<div class="rate">
							<input type="radio" id="star5" ng-model="rating.rating" value="5" />
							<label for="star5" title="text">5 stars</label>
							<input type="radio" id="star4" ng-model="rating.rating" value="4" />
							<label for="star4" title="text">4 stars</label>
							<input type="radio" id="star3" ng-model="rating.rating" value="3" />
							<label for="star3" title="text">3 stars</label>
							<input type="radio" id="star2" ng-model="rating.rating" value="2" />
							<label for="star2" title="text">2 stars</label>
							<input type="radio" id="star1" ng-model="rating.rating" value="1" />
							<label for="star1" title="text">1 star</label>
						</div>
					</div>
					<br>
					<p class="help-block" ng-if="errors.rating">{{errors.rating[0]}}</p>
				</div>
				<div ng-class="errors.content?'has-error form-group':'form-group'">
					<label>Nội dung đánh giá</label>
					<textarea cols="100%" rows="5" class="form-control" ng-model="rating.content"></textarea>					
					<p class="help-block" ng-if="errors.content">{{errors.content[0]}}</p>
				</div>
				<div class="form-group">
					<button class="btn btn-sm btn-secondary" ng-click="ok()">Gửi nội dung</button>
				</div>
		<div class="clearfix"></div>
	</div><!-- modal-body -->
</script>