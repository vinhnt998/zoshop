<?php

namespace frontend\widgets;

class Modal extends \yii\base\Widget
{

    public function run(){
        return $this->render('modal');
    }

}
